<#--
/****************************************************
 * Description: t_mall_order_shipping的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>订单ID</th>
	        <th>收货人全名</th>
	        <th>固定电话</th>
	        <th>移动电话</th>
	        <th>省份</th>
	        <th>城市</th>
	        <th>区/县</th>
	        <th>收货地址，如：xx路xx号</th>
	        <th>邮政编码,如：310001</th>
	        <th>created</th>
	        <th>updated</th>
	        <th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.orderId}
			</td>
			<td>
			    ${item.receiverName}
			</td>
			<td>
			    ${item.receiverPhone}
			</td>
			<td>
			    ${item.receiverMobile}
			</td>
			<td>
			    ${item.receiverState}
			</td>
			<td>
			    ${item.receiverCity}
			</td>
			<td>
			    ${item.receiverDistrict}
			</td>
			<td>
			    ${item.receiverAddress}
			</td>
			<td>
			    ${item.receiverZip}
			</td>
			<td>
			    ${item.created?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
			    ${item.updated?string('yyyy-MM-dd HH:mm:ss')}
			</td>
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/mall/order/shipping/input/${item.id}','修改t_mall_order_shipping','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/mall/order/shipping/delete/${item.id}','删除t_mall_order_shipping？',false,{id:'${tabId}'});">删除</@button>
            </td>
		</tr>
		</#list>
	</tbody>
</@list>