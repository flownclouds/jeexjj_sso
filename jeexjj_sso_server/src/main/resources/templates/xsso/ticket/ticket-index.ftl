<#include "/templates/xjj-index.ftl">
<#--导航-->
<@navList navs=navArr/>
<script>
	function kickUser(gt)
	{
		XJJ.confirm("强制下线用户?", function(){
			$.ajax({
				type: "post",
				url: '${base}/xsso/ticket/kick/'+gt,
				success: function(data){
				  	if(data.message != '' && data.type == 'success'){
						XJJ.msg(data.message);
						
						$("#"+gt).remove();
					}else{
						XJJ.msger(data.message);
					}
					var $modal = $("#bsmodal");
					$modal.modal("hide");
				}
			});
		});
	}
</script>
<@content>
	<table class="table table-bordered table-hover dataTable no-footer">
	<thead>
		<tr>
			<th>用户</th>
			<th>登陆项目</th>
			<th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list gtList?if_exists as gticket>
		<tr id="${gticket.id}">
			<td>
			名称：${gticket.user?if_exists.userName}<br/>
	    	时间：${gticket.creationTime?string('yyyy-MM-dd HH:mm:ss')}<br/>
			票据：${gticket.id}
			</td>
			<td>
			<#list gticket.serviceTicketMap?keys as serviceTicketId>
			${gticket.serviceTicketMap[serviceTicketId].host}
			<font color="green">
			[${gticket.serviceTicketMap[serviceTicketId].creationTime?string('yyyy-MM-dd HH:mm:ss')}]
			</font>
			<font color="danger">
			[来自：${gticket.serviceTicketMap[serviceTicketId].projectCode}]
			</font>
			<br/>
			</#list>
			</td>
			<td>
				<@button type="danger" icon="fa fa-user" onclick="kickUser('${gticket.id}');">强制下线</@button>
			</td>
		</tr>
		</#list>
	</tbody>
	</table>
	最近在线人数：<font color="green"><b>${gtList?if_exists?size}</b></font>人
</@content>