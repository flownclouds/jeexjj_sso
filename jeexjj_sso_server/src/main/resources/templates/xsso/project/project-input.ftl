<#--
/****************************************************
 * Description: t_sso_project的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-05-09 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/xsso/project/save" id=tabId>
   <input type="hidden" name="id" value="${project.id}"/>
   
   <@formgroup title='名称'>
	<input type="text" name="name" value="${project.name}" >
   </@formgroup>
   <@formgroup title='编码'>
	<input type="text" name="code" value="${project.code}" >
   </@formgroup>
   <@formgroup title='登陆地址'>
	<input type="text" name="loginUrl" value="${project.loginUrl}" >
   </@formgroup>
   <@formgroup title='有效IP'>
	<input type="text" name="validIp" value="${project.validIp}" >
   </@formgroup>
   <@formgroup title='密码方式'>
	<input type="text" name="pwdType" value="${project.pwdType}" >
   </@formgroup>
   <@formgroup title='状态'>
	<@swichInForm name="status" val=project.status onTitle="有效" offTitle="无效"></@swichInForm>
   </@formgroup>
</@input>