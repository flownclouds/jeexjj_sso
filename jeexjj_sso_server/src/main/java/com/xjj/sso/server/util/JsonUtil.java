package com.xjj.sso.server.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;



public class JsonUtil {
	
	private static  ObjectMapper mapper = new ObjectMapper();  
	
	public static String toJSONString(Object obj)
	{
		String json = null;;
		try {
			json = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}  
		return json;
	}
}
