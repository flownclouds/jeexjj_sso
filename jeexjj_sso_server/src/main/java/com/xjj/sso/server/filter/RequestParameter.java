package com.xjj.sso.server.filter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.xjj.sso.server.util.SSOUtils;

public class RequestParameter {
    
	private static SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
    private String code;
    private String action;
    private String method;
    private Map<String, String[]> requestPram;
    
    
    /**
     * 生成参数标识
     * @return
     */
    public static String generateCode()
    {
    	String time = sdf.format(new Date());
    	int round = (int)(Math.random()*100);
    	return time+round;
    }
    
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public Map<String, String[]> getRequestPram() {
		return requestPram;
	}
	public void setRequestPram(Map<String, String[]> requestPram) {
		this.requestPram = requestPram;
	}
	
	public String getParameter(String key)
	{
		if(null == requestPram || SSOUtils.isBlank(key))
		{
			return null;
		}
		
		String[] values = requestPram.get(key);
		if(null == values)
		{
			return null;
		}
		
		return values[0];
	}
	
	public static void main(String[] args) {
		int round = (int)(Math.random()*100);
		System.out.println(round);
	}
}