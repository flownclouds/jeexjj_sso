package com.xjj.sso.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/passport")
public class PassportController {

	@RequestMapping("/login")
    public String login(Model model) {
        return "/passport/login";
    }
	@RequestMapping("/jsonp")
	public String jsonp(Model model) {
		model.addAttribute("zhj","zhanghejie");
		return "/passport/jsonp";
	}
}
