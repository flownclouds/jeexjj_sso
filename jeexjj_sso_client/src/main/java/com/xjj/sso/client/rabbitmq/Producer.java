package com.xjj.sso.client.rabbitmq;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer extends EndPoint{
	
	
	public Producer(String endPointName) throws IOException, TimeoutException{
        super(endPointName);
        
        //发布广播消息
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
    }
 
    public void sendMessage(byte[] msg) throws Exception {
        channel.basicPublish(EXCHANGE_NAME,endPointName, null, msg);
        close();
    }
    
    
}
