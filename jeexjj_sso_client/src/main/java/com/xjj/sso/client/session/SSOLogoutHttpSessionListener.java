package com.xjj.sso.client.session;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import com.xjj.sso.client.filter.SSOClientFilter;
import com.xjj.sso.client.util.SSOUtils;

public final class SSOLogoutHttpSessionListener implements HttpSessionListener {

	
    public void sessionCreated(final HttpSessionEvent event) {
        // nothing to do at the moment
    }

    public void sessionDestroyed(final HttpSessionEvent event) {
        final HttpSession session = event.getSession();
        String ticket = SSOClientFilter.SESSION_TICKET_CACHE.get(session.getId());
        
        if(SSOUtils.isNotBlank(ticket))
        {
        	//session失效清除相关缓存
        	SSOClientFilter.SESSION_TICKET_CACHE.remove(session.getId());
        	SSOClientFilter.TICKET_SESSION_CACHE.remove(ticket);
        }
    }
}
