package com.xjj.sso.client.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.xjj.sso.client.user.SSOReceipt;


public interface SessionHandle {

	public void handle(HttpServletRequest request,SSOReceipt receipt);
	public void ssoLogout(String ticketId,HttpSession session);
	
}
